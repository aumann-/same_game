#pragma once

#include "SameGameBoard.h"

class CSameGameDoc : public CDocument
{
	protected: // create from serialization only
	  CSameGameDoc();
	  virtual ~CSameGameDoc();
	  DECLARE_DYNCREATE(CSameGameDoc)

	// Attributes
	public:

	  // Operations
	public:

	  /*  Functions for accessing the game board */
	  COLORREF GetBoardSpace(int row, int col)
	  { return m_board.GetBoardSpace(row, col); }

	  void SetupBoard()   { m_board.SetupBoard(); }
	  int GetWidth()      { return m_board.GetWidth(); }
	  int GetHeight()     { return m_board.GetHeight(); }
	  int GetColumns()    { return m_board.GetColumns(); }
	  int GetRows()       { return m_board.GetRows(); }
	  void DeleteBoard()  { m_board.DeleteBoard(); }


	  // Overrides
	public:
	  virtual BOOL OnNewDocument();

	protected:

	  /*  Instance of the game board */
	  CSameGameBoard m_board;


	  // Generated message map functions
	protected:
	  DECLARE_MESSAGE_MAP()
};