This is the SameGame project done using the tutorial at http://www.cprogramming.com/tutorial/game_programming/same_game_part1.html.

This program is being made in Visual Studio 2010 for practice.

The point of the game is to try to remove all the colored blocks by clicking on any block that is next to one of the same color, causing those blocks to disappear.


10/30/13: Created game board (Part 1 of tutorial)